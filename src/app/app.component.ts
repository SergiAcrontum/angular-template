import { Component } from '@angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'angular-template'

  async fn (): Promise<any> {
    return await Promise.resolve()
  }
}
